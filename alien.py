class Alien():
    def __init__(self, x, y):
        self._x = x
        self._y = y
        self._salud = 3
        
    def x(self):
        return self._x

    def y(self):
        return self._y

    def golpe(self):
        self._salud = self._salud - 1

        if self._salud < 0:
            print("El alien se murio")   
        else:
            return self._salud
        
    def vive(self):
        if self._salud > 0:
            return True
        else:
            return False

    def teleport(self, x, y):
        self._x = x
        self._y = y

        return self._x, self._y 


#alien = Alien(8,3)